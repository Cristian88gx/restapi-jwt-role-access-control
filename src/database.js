const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/bd_role_access', {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(db => console.log('Database is connected'))